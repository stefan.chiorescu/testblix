package com.stefan.blixmusicplayer.ui

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.stefan.blixmusicplayer.adapters.AlbumsAdapter
import com.stefan.blixmusicplayer.R
import com.stefan.blixmusicplayer.databinding.AlbumsFragmentBinding
import com.stefan.blixmusicplayer.di.AlbumsAdapterFactory
import com.stefan.blixmusicplayer.models.Albums
import com.stefan.blixmusicplayer.models.Song
import com.stefan.blixmusicplayer.viewmodel.MediaPlayerViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.ArrayList
import javax.inject.Inject


@AndroidEntryPoint
class AlbumsFragment : Fragment(R.layout.albums_fragment) {

    private var _binding: AlbumsFragmentBinding? = null

    private val binding get() = _binding!!

    @Inject
    lateinit var adapterFactory: AlbumsAdapterFactory

    private val mediaPlayerViewModel: MediaPlayerViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AlbumsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewState()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private fun setupAdapter(list: List<Albums>, onClick: (Albums) -> Unit): AlbumsAdapter {
        val adapter = adapterFactory.create(onClick)
        adapter.differ.submitList(list)
        return adapter
    }

    private fun observeViewState() {
        mediaPlayerViewModel.albumsList.observe(this) {
            binding.recyclerViewSongsList.apply {
                layoutManager = LinearLayoutManager(this@AlbumsFragment.requireContext())
                adapter = setupAdapter(it) {
                    val bundle = Bundle()
                    bundle.apply {
                        putParcelableArray(SONGS, ArrayList(it.songList).toTypedArray())
                    }
                    findNavController().navigate(R.id.action_AlbumsFragment_to_SongsFragment, bundle)

                }
            }
        }

    }

    companion object {

        const val SONGS = "songs"
    }

}