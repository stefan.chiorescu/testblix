package com.stefan.blixmusicplayer.ui

import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.stefan.blixmusicplayer.R
import com.stefan.blixmusicplayer.adapters.AlbumsAdapter
import com.stefan.blixmusicplayer.adapters.SongsAdapter
import com.stefan.blixmusicplayer.databinding.SongsFragmentBinding
import com.stefan.blixmusicplayer.di.AlbumsAdapterFactory
import com.stefan.blixmusicplayer.di.SongsAdapterFactory
import com.stefan.blixmusicplayer.models.Albums
import com.stefan.blixmusicplayer.models.Song
import com.stefan.blixmusicplayer.ui.AlbumsFragment.Companion.SONGS
import com.stefan.blixmusicplayer.viewmodel.MediaPlayerViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SongsFragment : Fragment(R.layout.songs_fragment) {

    private var _binding: SongsFragmentBinding? = null

    private val binding get() = _binding!!

    @Inject
    lateinit var songsAdapterFactory: SongsAdapterFactory

    private val mediaPlayerViewModel: MediaPlayerViewModel by viewModels({ requireParentFragment()})

    private var songsList: ArrayList<Song> = arrayListOf()


    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SongsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

      //  arguments?.getParcelableArray(SONGS)

        //Retrieve the songs and pass them to the adapter
        arguments?.getParcelableArray(SONGS)


        setupRecyclerView()
    }

    private fun setupAdapter(list: List<Song>, onClick: (Song) -> Unit): SongsAdapter {
        val adapter = songsAdapterFactory.create(onClick)
        adapter.differ.submitList(list)
        return adapter
    }

    private fun setupRecyclerView() {
        binding.recyclerViewSongsList.apply {
            layoutManager = LinearLayoutManager(this@SongsFragment.requireContext())
           // adapter = setupAdapter() {

           // }
        }
    }
}