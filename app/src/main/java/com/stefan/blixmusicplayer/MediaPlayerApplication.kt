package com.stefan.blixmusicplayer

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MediaPlayerApplication: Application() {
}