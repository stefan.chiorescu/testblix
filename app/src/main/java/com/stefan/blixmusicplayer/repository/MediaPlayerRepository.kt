package com.stefan.blixmusicplayer.repository

import com.stefan.blixmusicplayer.models.Albums
import com.stefan.blixmusicplayer.models.Song
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class MediaPlayerRepository @Inject constructor() {

    suspend fun getAlbums(): Flow<List<Albums>> = flowOf(albumsList)

    companion object {

        private val songList: List<Song> = listOf(
            Song(name = "Song1", duration = 180 , artist = "Artist1"),
            Song(name = "Song2", duration = 180 , artist = "Artist1"),
            Song(name = "Song3", duration = 180 , artist = "Artist1"),
            Song(name = "Song4", duration = 180 , artist = "Artist1"),
            Song(name = "Song5", duration = 180 , artist = "Artist1"),
            Song(name = "Song6", duration = 180 , artist = "Artist1"),
            Song(name = "Song7", duration = 180 , artist = "Artist1"),
            Song(name = "Song8", duration = 180 , artist = "Artist1"),
            Song(name = "Song9", duration = 180 , artist = "Artist1"),
            Song(name = "Song10", duration = 180 , artist = "Artist1"),
            Song(name = "Song11", duration = 180 , artist = "Artist1"),
        )


        val albumsList: List<Albums> = listOf(
            Albums(name = "Album1", songList = songList),
            Albums(name = "Album2", songList = songList),
            Albums(name = "Album3", songList = songList),
            Albums(name = "Album4", songList = songList),
            Albums(name = "Album5", songList = songList),
            Albums(name = "Album6", songList = songList),
            Albums(name = "Album7", songList = songList),
            Albums(name = "Album8", songList = songList),
            Albums(name = "Album9", songList = songList),
        )



    }
}