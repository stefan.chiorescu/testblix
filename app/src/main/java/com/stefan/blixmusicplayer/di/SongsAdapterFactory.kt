package com.stefan.blixmusicplayer.di

import com.stefan.blixmusicplayer.adapters.SongsAdapter
import com.stefan.blixmusicplayer.models.Song
import dagger.assisted.AssistedFactory

@AssistedFactory
interface SongsAdapterFactory {

    fun create(onClick : (Song) -> Unit) : SongsAdapter
}