package com.stefan.blixmusicplayer.di

import com.stefan.blixmusicplayer.adapters.AlbumsAdapter
import com.stefan.blixmusicplayer.models.Albums
import dagger.assisted.AssistedFactory

@AssistedFactory
interface AlbumsAdapterFactory {

    fun create(onClick : (Albums) -> Unit) : AlbumsAdapter
}