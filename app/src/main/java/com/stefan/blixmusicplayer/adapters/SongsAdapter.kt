package com.stefan.blixmusicplayer.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.stefan.blixmusicplayer.databinding.SongItemBinding
import com.stefan.blixmusicplayer.models.Song
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject

class SongsAdapter @AssistedInject constructor(
    @Assisted val onClick: (Song) -> Unit
) : RecyclerView.Adapter<SongsAdapter.SongsViewHolder>() {

    private lateinit var rowItemBinding: SongItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongsViewHolder {
        rowItemBinding = SongItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SongsViewHolder()
    }

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: SongsViewHolder, position: Int) {
        holder.bind(differ.currentList[position], onClick)
    }

    inner class SongsViewHolder : RecyclerView.ViewHolder(rowItemBinding.root) {

        fun bind(song: Song, onClick: (Song) -> Unit) {
            rowItemBinding.apply {
                textviewSongName.text = song.name
                textViewDuration.text = song.duration.toString()
                executePendingBindings()
            }
            rowItemBinding.root.setOnClickListener {
                onClick(song)
            }
        }
    }

    private val differCallback = object : DiffUtil.ItemCallback<Song>() {
        override fun areItemsTheSame(oldItem: Song, newItem: Song): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: Song, newItem: Song): Boolean {
            return oldItem == newItem
        }

    }
    val differ = AsyncListDiffer(this, differCallback)
}