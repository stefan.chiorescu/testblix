package com.stefan.blixmusicplayer.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.stefan.blixmusicplayer.databinding.AlbumsItemBinding
import com.stefan.blixmusicplayer.models.Albums
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject

class AlbumsAdapter @AssistedInject constructor(
    @Assisted val onClick: (Albums) -> Unit
) : RecyclerView.Adapter<AlbumsAdapter.AlbumsViewHolder>() {

    private lateinit var rowItemBinding: AlbumsItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumsViewHolder {
        rowItemBinding = AlbumsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AlbumsViewHolder()
    }

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: AlbumsViewHolder, position: Int) {
        holder.bind(differ.currentList[position], onClick)
    }

    inner class AlbumsViewHolder : RecyclerView.ViewHolder(rowItemBinding.root) {

        fun bind(album: Albums, onClick: (Albums) -> Unit) {
            rowItemBinding.apply {
                textviewAlbumName.text = album.name
                executePendingBindings()
            }
            rowItemBinding.root.setOnClickListener {
                onClick(album)
            }
        }
    }

    private val differCallback = object : DiffUtil.ItemCallback<Albums>() {
        override fun areItemsTheSame(oldItem: Albums, newItem: Albums): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: Albums, newItem: Albums): Boolean {
            return oldItem == newItem
        }

    }
    val differ = AsyncListDiffer(this, differCallback)
}
