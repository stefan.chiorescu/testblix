package com.stefan.blixmusicplayer.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.stefan.blixmusicplayer.models.Albums
import com.stefan.blixmusicplayer.repository.MediaPlayerRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MediaPlayerViewModel @Inject constructor(
    private val repository: MediaPlayerRepository
) : ViewModel() {

    private var _albumsList = MutableLiveData<List<Albums>>()
    val albumsList: LiveData<List<Albums>>
    get() = _albumsList


    init {
        getAlbums()
    }

    private fun getAlbums() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getAlbums()
                .collect{
                    _albumsList.postValue(it)
                }
        }
    }
}