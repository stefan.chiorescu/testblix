package com.stefan.blixmusicplayer.models

import android.os.Parcel
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
data class Albums(
    val name: String,
    val songList: List<Song>
): Parcelable


@Parcelize
data class Song(
    val name: String,
    val duration: Int,
    val artist: String
): Parcelable